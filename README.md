Falldown is a cartoon game where you have to tilt your phone to make ball fall
down quickly with rhythmic music. Don't get squashed!

# Build and run  
Install [clickable](https://github.com/bhdouglass/clickable).

    git clone git@gitlab.com:jonnius/falldown.git
    cd falldown
    clickable # to build for phablet
    clickable desktop # to build for desktop

See [clickable documentation](http://clickable.bhdouglass.com/) for details.

# Credits
This is a fork from [Falldown by Riccardo Padovani](https://launchpad.net/falldown).
