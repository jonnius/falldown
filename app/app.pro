TEMPLATE = app
TARGET = falldown

QT += qml quick

SOURCES += main.cpp

RESOURCES += falldown.qrc

OTHER_FILES += falldown.apparmor \
               falldown.desktop \
               falldown.png \
               themes/default/sounds/soundtrack.mp3

#specify where the config files are installed to
config_files.path = /falldown
config_files.files += $${OTHER_FILES}
message($$config_files.files)
INSTALLS+=config_files

# Default rules for deployment.
include(../deployment.pri)
