/*
* Copyright 2015 Riccardo Padovani <riccardo@rpadovani.com>
*
* This file is part of falldown.
*
* falldown is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; version 3.
*
* falldown is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

var colors = {
  default: {
    subtitleText: 'black',
    usualText: '#0072B2',
    endGameText: '#d55e00',
    versionGameText: '#E69F00',
    scoreText: 'black',
    starText: 'black',
    achievementTitleLocked: 'black',
    achievementTitleUnlocked: '#d55e00',
    achievementTitleCollected: '#0072B2',
    achievementDescription: 'black',
  },
  alt: {
    subtitleText: '#ecf0f1',
    usualText: '#0072B2',
    endGameText: '#d55e00',
    versionGameText: '#E69F00',
    scoreText: 'black',
    starText: 'white',
    achievementTitleLocked: 'white',
    achievementTitleUnlocked: '#d55e00',
    achievementTitleCollected: '#0072B2',
    achievementDescription: 'white',
  },
};
