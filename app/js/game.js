/*
 * Original work Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
 * Copyright 2019 Jonatan Hatakeyama Zeidler <jonatan_zeidler@gmx.de>
 *
 * This file is part of falldown.
 *
 * falldown is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * falldown is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

var BASIC_POSSIBILITY_OF_POWERUP = 0.07;
var PERCENTUAL_INCREMENTS_FOR_EACH_FLOOR_WITHOUT_POWERUP = 0.01;
var PERCENTUAL_OF_POSITIVE_POWERUPS = 0.65;
var PERCENTUAL_OF_TWO_HOLES = 0.1;

var UPPER_LIMIT_THREE_BALLS = 0.2;
var UPPER_LIMIT_SMALLER_BALL = 0.6;
var UPPER_LIMIT_SLOW_DOWN = 0.9;
var UPPER_LIMIT_EXTRA_LIFE = 1;

var UPPER_LIMIT_BALLOON = 0.45;
var UPPER_LIMIT_GLUE = 0.9;
var UPPER_LIMIT_WINE = 1;

var howManyFloorsWithoutPowerUp = 0;
var floorMutex = false;

function addFloor(width, gu) {
    // Be sure we don't duplicate floors
    // https://bugs.launchpad.net/falldown/+bug/1495066
    if (floorMutex) return;
    floorMutex = true;

    // Choose number of holes in the floor, 90% of times just one
    var numberOfHoles = Math.random() < PERCENTUAL_OF_TWO_HOLES ? 2 : 1;

    // Let's see if we have a power up
    var newPowerUp = Math.random() <= BASIC_POSSIBILITY_OF_POWERUP + howManyFloorsWithoutPowerUp * PERCENTUAL_INCREMENTS_FOR_EACH_FLOOR_WITHOUT_POWERUP ? true : false;
    if (newPowerUp) {
        addPowerup();
        howManyFloorsWithoutPowerUp = 0;
    } else {
        howManyFloorsWithoutPowerUp++;
    }

    var firstHolePosition = randomIntFromInterval(0, numberOfHoles === 2 ? width/2 : width - gu * 7);

    if (numberOfHoles === 1) {
        var newPlane = planeStatic.createObject(gameScene);
        newPlane.x = 0;
        newPlane.y = gameScene.height;
        newPlane.width = gameScene.width;
        newPlane.holePosition = firstHolePosition;
    } else {
        var thp = twoHolesPlaneStatic.createObject(gameScene);
        thp.x = 0;
        thp.y = gameScene.height;
        thp.width = gameScene.width;
        thp.firstHolePosition = firstHolePosition;
        thp.secondHolePosition = randomIntFromInterval(firstHolePosition + gu * 9, width - gu * 7);
    }

    floorMutex = false;
}

function addBall(x, y) {
    var newBall = ballStatic.createObject(gameScene);
    newBall.x = x || gameScene.width / 2;
    newBall.y = y || 0;
    gameScene.numberOfBalls++;

    if (gameScene.numberOfBalls >= 10) {
        game.unlockAchievements(['clonesBrothers', 'multiply'])
    } else if (gameScene.numberOfBalls >= 6) {
        game.unlockAchievements(['multiply'])
    }
}

function addPowerup() {
    var newPowerUp = powerUpStatic.createObject(gameScene);
    var isPositivePowerUp = Math.random() <= PERCENTUAL_OF_POSITIVE_POWERUPS;
    var typeOfPowerUp = Math.random();

    if (isPositivePowerUp) {
        if (typeOfPowerUp < UPPER_LIMIT_THREE_BALLS) {
            newPowerUp.typeOfPowerUp = 'threeBalls';
        } else if (typeOfPowerUp < UPPER_LIMIT_SMALLER_BALL) {
            newPowerUp.typeOfPowerUp = 'smallBall';
        } else if (typeOfPowerUp < UPPER_LIMIT_SLOW_DOWN) {
            newPowerUp.typeOfPowerUp = 'slowTime';
        } else if (gameScene.lifes < 3) {
            newPowerUp.typeOfPowerUp = 'heart';
        } else {
            // No hearts for you, sorry!
            newPowerUp.typeOfPowerUp = 'threeBalls';
        }
    } else {
        if (typeOfPowerUp < UPPER_LIMIT_BALLOON) {
            newPowerUp.typeOfPowerUp = 'balloon';
        } else if (typeOfPowerUp < UPPER_LIMIT_GLUE) {
            newPowerUp.typeOfPowerUp = 'glue';
        } else {
            newPowerUp.typeOfPowerUp = 'wine';
        }
    }
    newPowerUp.x = randomIntFromInterval(0, gameScene.width / 1.2);
    newPowerUp.y = gameScene.height + units.gu(2);
}

// Credits to http://stackoverflow.com/a/7228322/2586392
function randomIntFromInterval(min, max) {
    if (max < min) return max;
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function startGame() {
    gameScene.endGame = false;
    resetGame();
    game.currentScene = gameScene;
    game.gameState = Bacon2D.Running;
    Game.addFloor(parent.width, units.gu(1));
    Game.addBall();
}

function resetGame() {
    gameScene.gravity = Qt.point(0, 20);
    gameScene.score = 0;
    mainView.velocity = units.gu(0.3);
    gameScene.lifes = 2;
    gameScene.numberOfBalls = 0;
    gameScene.numberOfHolesWithoutLosingALife = 0;
    resetPowerupsEffects();
}

function endGame() {
    gameScene.endGame = true;
    gameScene.numberOfHolesAfterWine = -1;
    game.currentScene = endScene;
    endScene.lastScore = score;
    settings.numberOfPlayedGames++;
    if (settings.numberOfPlayedGames >= 100) {
        game.unlockAchievements(['start', 'addicted', 'stop']);
    } else if (settings.numberOfPlayedGames >= 50) {
        game.unlockAchievements(['start', 'addicted']);
    } else if (settings.numberOfPlayedGames >= 10) {
        game.unlockAchievements(['start']);
    }
    if (gameScene.score > settings.highScore) {
        settings.highScore = gameScene.score;
    }
    if (gameScene.deadAfterWine) {
        game.unlockAchievements(['drinking']);
        gameScene.deadAfterWine = false;
    }
    game.reloadAchievements();
    resetGame();
}

function restartGame() {
    gameScene.endGame = true;
    startGame();
}

function resetPowerupsEffects() {
    gameScene.smallerBall = false;
    gameScene.slowTime = false;
    gameScene.balloon = false;
    gameScene.glue = false;
    gameScene.wine = false;
}
